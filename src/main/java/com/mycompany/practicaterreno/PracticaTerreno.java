/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.practicaterreno;

/**
 *
 * @author Alexis
 */
public class PracticaTerreno {

     public static void main(String[] args) {
        // TODO code application logic here
        
        //contructor por omision
        Terreno terreno = new Terreno();
        
        terreno.setAncho(10.50f);        
        terreno.setLargo(20.00f);
        
        System.out.println("perimetro es igual a:" + terreno.calculadoraPerimetro());
        System.out.println("Area es igual a: "+ terreno.calculadoraArea());
        
        Terreno ter = new Terreno(10,20.20f,40.00f);
        
        System.out.println("lo ancho es: "+ter.getAncho());
        
        Terreno ter2 = new Terreno(terreno);
        System.out.println("perimetro es igual a:" + ter2.calculadoraPerimetro());
        System.out.println("Area es igual a: "+ ter2.calculadoraArea());
        
        
    }
    
}
