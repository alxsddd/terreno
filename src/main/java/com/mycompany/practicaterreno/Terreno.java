/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.practicaterreno;

/**
 *
 * @author Alexis
 */
class Terreno {
    
    private int numTerreno;
    private float ancho;
    private float largo;
            
 //constructores            
public Terreno(){

this.numTerreno=0;
this.ancho=0.0f;
this.largo=0.0f;
}
public Terreno( int numTerreno, float ancho, float largo){

    this.numTerreno=numTerreno;
    this.ancho=ancho;
    this.largo=largo;
}
public Terreno(Terreno otro){
    this.numTerreno=otro.numTerreno;
    this.largo=otro.largo;
    this.ancho=otro.ancho;
}

//metodos get y set 
    public int getNumTerreno() {
        return numTerreno;
    }

    public void setNumTerreno(int numTerreno) {
        this.numTerreno = numTerreno;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }

public float calculadoraPerimetro(){
float perimetro=0.0f;
perimetro = (this.ancho + this.largo)*2;
return perimetro;
}

public float calculadoraArea(){
float area = 0.0f;
area = this.ancho * this.largo;
return area;
}


}
    

   
